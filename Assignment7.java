
package javaapplication8;

import java.util.ArrayList;
import java.util.Scanner;


public class Employee {
        
    int id;
    String name;
    String address;
    int sal;
    
    public Employee(int id,String name, String address, int sal)
    {
        this.id=id;
        this.sal=sal;
        this.name=name;
        this.address=address;
    }
    
    public static void main(String[] args) {
        ArrayList<Employee> list=new ArrayList<Employee>();
        list.add(new Employee(2000,"nitesh","bengaluru",20000));
        list.add(new Employee(2001,"karan","chennai",15000));
        list.add(new Employee(2002,"vipul","manali",300000));
        list.add(new Employee(2003,"simran","shimla",40000));
        list.add(new Employee(2004,"nitesh","chandigarh",50000));
        list.add(new Employee(2005,"priya","jaipur",34000));
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the name :: ");
        String name=sc.next();
        
        System.out.println("Enter the id :: ");
        int id=sc.nextInt();
        
        for( Employee e: list)
        {
            if(e.name.equals(name) && e.id==id)
            {
                System.out.println("Here's the record you've been looking for :: \n"+e.id+"\t"+e.name+"\t"+e.sal+"\t"+e.address);
            }
        }
    }
    
}

