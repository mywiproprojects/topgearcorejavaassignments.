
package j3;

abstract class Instrument{
    abstract String play();
}
class Piano extends Instrument{
    @Override
    public String play(){
        return "Piano is playing  tan tan tan tan";
    }
}
class Flute extends Instrument{
    @Override
    public String play(){
        return "Flute is playing  toot toot toot toot";
    }
}
class Guitar extends Instrument{
    @Override
    public String play(){
        return "Guitar is playing  tin  tin  tin";
    }
}
public class MyMusicalInstruments {

    
    public static void main(String[] args) {
        Instrument instrument[]=new Instrument[10];
        instrument[0]=new Piano();
        instrument[1]=new Guitar();
        instrument[2]=new Flute();
        instrument[3]=new Guitar();
        instrument[4]=new Flute();
        instrument[5]=new Guitar();
        instrument[6]=new Piano();
        instrument[7]=new Guitar();
        instrument[8]=new Piano();
        instrument[9]=new Flute();
        for(int i=0;i<10;i++)
        {
            System.out.println(instrument[i].play()+"\n");
        }
        
    }
    
}

