
package javaapplication6;

class MyException extends Exception
{
    public MyException(String age)
    {
        super(age);
    }
}
public class ExceptionHandling {

   
    public static void main(String[] args) throws MyException {
        String name=args[0];
        int age=Integer.parseInt(args[1]);
        try{
        if(age<18 || age>60)
            throw new MyException("Invalid Age");
        
    }
        catch(MyException e)
        {
            System.out.print(e);
        }
    }
    
}

