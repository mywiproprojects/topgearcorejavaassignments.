package j1;
import java.util.*;
public class FindTheFactorial {

    public static long factorial(long n)
    {
        if(n==1)
            return 1;
        else
            return n*factorial(n-1);
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the number you wanna find the factorial of: ");
        long n=sc.nextLong();
        System.out.println("Factorial of " +n+ " is: " +factorial(n));
    }
    
}
