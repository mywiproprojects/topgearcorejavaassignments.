package live;
import music.*;
import music.string.*;
import music.wind.*;


public class Test {

   
    public static void main(String[] args) {
        Veena veena=new Veena();
        Saxophone saxophone=new Saxophone();
        Playable p1=veena;
        Playable p2=saxophone;
        p1.play();
        p2.play();
    }
    
}
