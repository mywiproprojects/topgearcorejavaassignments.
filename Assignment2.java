package j2;
class Animal{
    public String shout()
    {
        return "shout-shout";
    }
}
class Dog extends Animal{
    public String shout()
    {return "Bhow-Bhow";}
}
class Horse extends Animal{
    public String shout()
    {return "Mhe-he-he-he-ew";}
}
class Cat extends Animal{
    public String shout()
    {return "Meow-Meow";}
}

public class AnimalMain   {

   
    
    public static void main(String[] args) {
        
        
        Animal animal=new Animal();
        Cat cat=new Cat();
        Dog dog=new Dog();
        Horse horse=new Horse();
        System.out.println(animal.shout()+cat.shout()+dog.shout()+horse.shout());
    }
    
}
